import java.util.Scanner;

public class subnet_cal {
    public static void main(String args[]) {
        Scanner kb = new Scanner(System.in);
        System.out.print("Enter the ip address: ");
        String ip = kb.nextLine();
        String split_ip[] = ip.split("\\."); // SPlit the string after every .
        String split_bip[] = new String[4]; // split binary ip
        String bip = "";

        for (int i = 0; i < 4; i++) {
            split_bip[i] = appendZeros(Integer.toBinaryString(Integer.parseInt(split_ip[i]))); // “18” => 18 => 10010 =>
                                                                                               // 00010010
            bip += split_bip[i];
        }
        System.out.println("IP in binary is " + bip);
        System.out.print("Enter the number of addresses: ");
        int n = kb.nextInt();

        // Calculation of mask
        String checkclass = ip.substring(0, 3);
        int cc = Integer.parseInt(checkclass);
        String mask = null;
        if (cc > 0) {
            if (cc <= 127) {
                mask = "255.0.0.0";
                System.out.println("Class A IP Address");
                System.out.println("SUBNET MASK: " + mask);
            }
            if (cc >= 128 && cc <= 191) {
                mask = "255.255.0.0";
                System.out.println("Class B IP Address");
                System.out.println("SUBNET MASK: " + mask);
            }
            if (cc >= 192 && cc <= 223) {
                mask = "255.255.255.0";
                System.out.println("Class C IP Address");
                System.out.println("SUBNET MASK: " + mask);
            }
            if (cc >= 224 && cc <= 239) {
                mask = "255.0.0.0";
                System.out.println("Class D IP Address Used for multicasting");
            }
            if (cc >= 240 && cc <= 254) {
                mask = "255.0.0.0";
                System.out.println("Class E IP Address Experimental Use");
            }
        }
        int bits = (int) Math.ceil(Math.log(n) / Math.log(2)); /*
                                                                * eg if address = 120, log 120/log 2 gives log to the
                                                                * base 2 => 6.9068, ceil gives us upper integer
                                                                */
        System.out.println("Number of bits required for address = " + bits);
        System.out.println("The subnet mask is = " + mask);

        // Calculation of first address and last address
        int fbip[] = new int[32];
        for (int i = 0; i < 32; i++)
            fbip[i] = (int) bip.charAt(i) - 48; // convert cahracter 0,1 to integer 0,1
        for (int i = 31; i > 31 - bits; i--)// Get first address by ANDing last n bits with 0
            fbip[i] &= 0;
        String fip[] = { "", "", "", "" };
        for (int i = 0; i < 32; i++)
            fip[i / 8] = new String(fip[i / 8] + fbip[i]);
        System.out.print("First address is = ");
        for (int i = 0; i < 4; i++) {
            System.out.print(Integer.parseInt(fip[i], 2));
            if (i != 3)
                System.out.print(".");
        }
        System.out.println();

        int lbip[] = new int[32];
        for (int i = 0; i < 32; i++)
            lbip[i] = (int) bip.charAt(i) - 48; // convert cahracter 0,1 to integer 0,1
        for (int i = 31; i > 31 - bits; i--)// Get last address by ORing last n bits with 1
            lbip[i] |= 1;
        String lip[] = { "", "", "", "" };
        for (int i = 0; i < 32; i++)
            lip[i / 8] = new String(lip[i / 8] + lbip[i]);
        System.out.print("Last address is = ");
        for (int i = 0; i < 4; i++) {
            System.out.print(Integer.parseInt(lip[i], 2));
            if (i != 3)
                System.out.print(".");
        }
        System.out.println();
    }

    static String appendZeros(String s) {
        String temp = new String("00000000");
        return temp.substring(s.length()) + s;
    }

}
